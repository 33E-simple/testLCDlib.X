/*! \file  linearizePosition.c
 *
 *  \brief Limit LCD positions to 0-31 and add 0x40 if over 15
 *
 *
 *  \author jjmcd
 *  \date October 5, 2015, 8:42 AM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */


/*! linearizePosition() - Correct LCD position to proper line */

/*! linearizePosition() takes an integer and presumes it refers to
 *  an LCD location.  The number is clipped to the range of 0..31 and
 *  then any number greater than 16 has 0x40 ORed in.  The result is
 *  that increasing addresses will display first on the top line of
 *  the display, wrap to the bottom line, and then wrap back to the
 *  top line.
 * 
 * \param p int - Position to linearize
 * \return int - Adjusted value to send to LCD
 *
 */
int linearizePosition(int p)
{
  p &= 0x1f;                /* Mask down to 0..31                   */
  if (p & 0x10)             /* Belong on second line?               */
    p = (p & 0x0f) | 0x40;  /* ORin second line bit                 */
  return p;
}
