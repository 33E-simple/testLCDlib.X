#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
# 256GM1 configuration
CND_ARTIFACT_DIR_256GM1=dist/256GM1/production
CND_ARTIFACT_NAME_256GM1=testLCDlib.X.production.hex
CND_ARTIFACT_PATH_256GM1=dist/256GM1/production/testLCDlib.X.production.hex
CND_PACKAGE_DIR_256GM1=${CND_DISTDIR}/256GM1/package
CND_PACKAGE_NAME_256GM1=testlcdlib.x.tar
CND_PACKAGE_PATH_256GM1=${CND_DISTDIR}/256GM1/package/testlcdlib.x.tar
# 32GM0 configuration
CND_ARTIFACT_DIR_32GM0=dist/32GM0/production
CND_ARTIFACT_NAME_32GM0=testLCDlib.X.production.hex
CND_ARTIFACT_PATH_32GM0=dist/32GM0/production/testLCDlib.X.production.hex
CND_PACKAGE_DIR_32GM0=${CND_DISTDIR}/32GM0/package
CND_PACKAGE_NAME_32GM0=testlcdlib.x.tar
CND_PACKAGE_PATH_32GM0=${CND_DISTDIR}/32GM0/package/testlcdlib.x.tar
# 128GM1 configuration
CND_ARTIFACT_DIR_128GM1=dist/128GM1/production
CND_ARTIFACT_NAME_128GM1=testLCDlib.X.production.hex
CND_ARTIFACT_PATH_128GM1=dist/128GM1/production/testLCDlib.X.production.hex
CND_PACKAGE_DIR_128GM1=${CND_DISTDIR}/128GM1/package
CND_PACKAGE_NAME_128GM1=testlcdlib.x.tar
CND_PACKAGE_PATH_128GM1=${CND_DISTDIR}/128GM1/package/testlcdlib.x.tar
