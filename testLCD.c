/*! \file  testLCD.c
 *
 *  \brief Exercise all features of the LCDlibrary
 *
 *
 *  \author jjmcd
 *  \date April 7, 2015, 3:16 PM
 */
/* Software License Agreement
 * Copyright (c) 2014 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#include "../include/LCD.h"
#include "../include/banner.h"

//! Fast RC oscillator with PLL
#pragma config FNOSC = FRCPLL
//! Watchdog timer off
#pragma config FWDTEN = OFF
//! Deadman timer off
#pragma config DMTEN = DISABLE

/*! LCDposition() message, will display right to left */
static char szPosMsg[2][32] ={
  "    Test of     ",
  " LCDposition()  "
};
/*! Positions for individual character position test */
int p1[20] = {1, 67, 15, 64, 4, 74, 8, 72, 1, 67, 15, 64, 4, 74, 8, 72, 1, 1, 1, 1};
/*! Positions to erase, same as p1 but shifted one position */
int p2[20] = {1, 1, 67, 15, 64, 4, 74, 8, 72, 1, 67, 15, 64, 4, 74, 8, 72, 1, 1, 1};
/*! Work string for display */
char szWork[32];

/*! Message 1 to display in random order to check LCDposition() */
char p3[33] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ012345";
/*! Message 2 to display in random order to check LCDposition() */
char p4[33] = "The rain in Spain stays mainly i";
/*! Message 3 to display in random order to check LCDposition() */
char p5[33] = "Fourscore and seven years ago, o";
/*! Message 4 to display in random order to check LCDposition() */
char p6[33] = "LCDposition()     test completed ";
/*! Remember which positions have been used in LCDposition() test */
int used[32];

/*! Initialize - Initialize the processor clock, ports and LCD */
void Initialize(void);
/*! linearizePosition - Correct LCD position to proper line */
int linearizePosition(int);
/*! LEDs() - Display a value in the LEDs */
void LEDs( int );

/*! main - Exercise the LCD */
/*! Application tests most features of the LCD library.  As each test
 *  is run, a number is displayed in binary on the LEDs to indicate
 *  progress.
 *
 *  Tests run:
 *  \li Complete character set
 *  \li Shift left
 *  \li Control cursor position
 *  \li Control cursor position and check individual erasing
 *  \li Shift right
 *  \li Cursor on
 *  \li Cursor left
 *  \li Cursor right
 *  \li Blink on, cursor off
 *  \li Blink on, cursor on.
 *
 */
int main(void)
{
  int pos,done;
  int i, j, k;
  char szWork[32];
  unsigned int useSpace[32];

  Initialize();
  srand(useSpace[31]);

  /* Display startup banner */
  LCDclear();
  LCDputs("    Exercise    ");
  LCDline2();
  LCDputs("      LCD       ");
  Delay_ms(1000);
  LCDclear();
  LCDputs("Compiled:");
  LCDposition(0x45);
  LCDputs(compDate(__DATE__));
  Delay_ms(1000);

  while (1)
    {
      /*------------------------------------------------------------*/
      /* Display complete character set, first slow then fast       */
      /*------------------------------------------------------------*/
      LEDs(1);
      LCDclear();
      LCDline2();
      LCDputs("'");
      LCDletter(0x20);
      LCDputs("' through '");
      LCDletter(0x7f);
      LCDputs("'");
      LCDhome();
      LCDputs(szPosMsg[0]);
      Delay_ms(1000);
      pos = 0;
      for (j = ' '; j < 128; j++)
        {
          LCDposition(linearizePosition(pos));
          pos++;
          LCDletter(j);
          Delay_ms(20);
        }
      for (k = 0; k < 5; k++)
        for (j = ' '; j < 128; j++)
          {
            LCDposition(linearizePosition(pos));
            pos++;
            LCDletter(j);
          }
      Delay_ms(1000);

      /*------------------------------------------------------------*/
      /* Display characters beyond 127                              */
      /*------------------------------------------------------------*/
      LCDclear();
      LCDposition(1);
      LCDputs("Show Characters");
      LCDposition(0x42);
      LCDputs("> 127 (0x7f)");
      Delay_ms(2000);

      LCDclear();
      for ( i=128; i<255; i+=4 )
        {
          LCDposition(1);
          LCDletter(i);
          sprintf(szWork,"%3d",i);
          LCDposition(0x40);
          LCDputs(szWork);
          Delay_ms(200);

          LCDposition(5);
          LCDletter(i+1);
          sprintf(szWork,"%3d",i+1);
          LCDposition(0x44);
          LCDputs(szWork);
          Delay_ms(200);

          LCDposition(9);
          LCDletter(i+2);
          sprintf(szWork,"%3d",i+2);
          LCDposition(0x48);
          LCDputs(szWork);
          Delay_ms(200);

          LCDposition(13);
          LCDletter(i+3);
          sprintf(szWork,"%3d",i+3);
          LCDposition(0x4c);
          LCDputs(szWork);
          Delay_ms(200);

//          Delay_ms(500);
        }

      /*------------------------------------------------------------*/
      /* Show off shift left                                        */
      /*------------------------------------------------------------*/
      LEDs(2);
      for (j = 0; j < 16; j++)
        {
          LCDshiftLeft();
          Delay_ms(50);
        }

      /*------------------------------------------------------------*/
      /* Exercise LCDposition                                       */
      /*------------------------------------------------------------*/
      LEDs(3);
      LCDclear();
      /* Display "Test of LCDposition" from right to left*/
      for (j = 15; j >= 0; j--)
        {
          LCDposition(0x40 + j);
          LCDletter(szPosMsg[1][j]);
          Delay_ms(50);
          LCDposition(j);
          LCDletter(szPosMsg[0][j]);
          Delay_ms(100);
        }
      Delay_ms(1000);

      /* Display A-Z,0-9 in random writing order */
      for (j = 0; j < 32; j++)
        used[j] = 0;
      LCDclear();
      done = 0;
      while (!done)
        {
          pos = rand() & 31;
          LCDposition(linearizePosition(pos));
          LCDletter(p3[pos]);
          used[pos] = 1;
          done = 1;
          for (j = 0; j < 32; j++)
            if (!used[j])
              done = 0;
        }
      Delay_ms(1000);

      /* Display "The rain in Spain stays mainly i" in random writing order */
      for (j = 0; j < 32; j++)
        used[j] = 0;
      done = 0;
      while (!done)
        {
          pos = rand() & 31;
          LCDposition(linearizePosition(pos));
          LCDletter(p4[pos]);
          used[pos] = 1;
          done = 1;
          for (j = 0; j < 32; j++)
            if (!used[j])
              done = 0;
        }
      Delay_ms(1000);

      /* Display "Fourscore and seven years ago, o" in random writing order */
      for (j = 0; j < 32; j++)
        used[j] = 0;
      done = 0;
      while (!done)
        {
          pos = rand() & 31;
          LCDposition(linearizePosition(pos));
          LCDletter(p5[pos]);
          used[pos] = 1;
          done = 1;
          for (j = 0; j < 32; j++)
            if (!used[j])
              done = 0;
        }
      Delay_ms(1000);

      /*------------------------------------------------------------*/
      /* LCD position with random letters erased                    */
      /*------------------------------------------------------------*/
      LEDs(4);
      LCDclear();
      for (j = 0; j < 20; j++)
        {
          LCDposition(p1[j]);
          LCDletter('A' + j);
          LCDposition(p2[j]);
          LCDletter(' ');
          Delay_ms(300);
        }

      /* Display "LCDposition()     test completed" in random writing order */
      for (j = 0; j < 32; j++)
        used[j] = 0;
      done = 0;
      while (!done)
        {
          pos = rand() & 31;
          LCDposition(linearizePosition(pos));
          LCDletter(p6[pos]);
          used[pos] = 1;
          done = 1;
          for (j = 0; j < 32; j++)
            if (!used[j])
              done = 0;
        }
      Delay_ms(1000);

      /*------------------------------------------------------------*/
      /* Show off shift right                                       */
      /*------------------------------------------------------------*/
      LEDs(5);
      for (j = 0; j < 16; j++)
        {
          LCDshiftRight();
          Delay_ms(50);
        }

      /*------------------------------------------------------------*/
      /* Cursor on                                                  */
      /*------------------------------------------------------------*/
      LEDs(6);
      LCDclear();
      LCDcursorOn();
      LCDputs("  Cursor On    ");
      Delay_ms(1000);

      /*------------------------------------------------------------*/
      /* Cursor Left                                                */
      /*------------------------------------------------------------*/
      LEDs(7);
      LCDhome();
      LCDputs("  Cursor Left   ");
      for ( j=0; j<16; j++ )
        {
          LCDleft();
          Delay_ms(250);
        }
      Delay_ms(1000);

      /*------------------------------------------------------------*/
      /* Cursor right                                               */
      /*------------------------------------------------------------*/
      LEDs(0);
      LCDhome();
      LCDputs("  Cursor Right   ");
      LCDposition(0);
      for ( j=0; j<15; j++ )
        {
          LCDright();
          Delay_ms(250);
        }
      Delay_ms(1000);

      /*------------------------------------------------------------*/
      /* Blink on                                                   */
      /*------------------------------------------------------------*/
      LEDs(1);
      LCDblinkOn();
      LCDclear();
      LCDputs("    Blink on   ");
      Delay_ms(1000);
      for ( j=0; j<15; j++ )
        {
          LCDleft();
          Delay_ms(500);
        }
      Delay_ms(1000);

      /*------------------------------------------------------------*/
      /* Blink on and cursor on                                     */
      /*------------------------------------------------------------*/
      LEDs(2);
      LCDcursorOff();
      LCDblinkAndCursor();
      LCDclear();
      LCDputs(" Blink & Cursor");
      Delay_ms(1000);
      for ( j=0; j<15; j++ )
        {
          LCDleft();
          Delay_ms(500);
        }
      Delay_ms(1000);
      LCDcursorOff();
    }
  return 0;
}

