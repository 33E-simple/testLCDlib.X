/*! \file  Initialize.c
 *
 *  \brief Initialize the processor clock, ports and LCD
 *
 *
 *  \author jjmcd
 *  \date June 9, 2015, 2:52 PM
 */
/* Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "../include/LCD.h"

/*! Initialize - Initialize the processor clock, ports and LCD */

/*!
 *
 */
void Initialize(void)
{

  // Wait a while to prevent programmer from doing weird stuff
  Delay_ms(2000L*23L/70L);  // Should be close to 2 second

  /* Select the proper oscillator and PLL settings for 70 MIPS */
  CLKDIVbits.FRCDIV = 0;    // Divide by 1 = 8MHz
  CLKDIVbits.PLLPRE = 0;    // Divide by 2 = 4 MHz
  PLLFBD = 74;              // Multiply by 70 = 140
  CLKDIVbits.PLLPOST = 0;   // Divide by 2 = 116

  /* Set the LED pins to output */
  _TRISB4 = 0;
  _TRISB6 = 0;
  _TRISB7 = 0;
  
  _TRISB8 = 0;

  _LATB6 = 1;
  LCDinit();
  _LATB6 = 0;

}
